import sys
import os
import time
from selenium import webdriver
import fitparse

import constants

if __name__ == "__main__":

    if len(sys.argv) < 2:
        raise ValueError("Missing Fit file name")

    fit_file_location = sys.argv[1]

    fit_file = fitparse.FitFile(fit_file_location)
    for message in fit_file.messages:
        if message.name == "sport":
            sport = message.get_value("sport")
            sub_sport = message.get_value("sub_sport")
            break
    activity = constants.SPORT_ANALYSER_DICT[sport][sub_sport](
        fit_file=fit_file, sport=sport, sub_sport=sub_sport
    )

    activity.draw_map()

delay = 5
map_name = "map.html"
url = "file://{path}/{mapfile}".format(path=os.getcwd(), mapfile=map_name)
browser = webdriver.Firefox()
browser.get(url)
# Give the map tiles some time to load
time.sleep(delay)
browser.save_screenshot("map.png")
browser.quit()
