class RecordPoint(object):
    """
    Class used to represent the uselful data retreive from FIT file

    Attributes
    ----------
    coordinate : dict
        The fit coordinates of the point
    decimal_coordinate : dict
        The decimal coordinates of the point
    speed : float
        The speed (in km/h) recorded at this point
    heart_rate : int
        The heart rate recorded at this point
    distance : float
        The distance covered at this point
    """

    @staticmethod
    def _conv_fit_coord_to_dec_coord(point):
        """
        Convert the fit point coordinate to a decimal coordinate

        Returns
        -------
        float
            The decimal coordinate of the point
        """
        return float(point) * (180.0 / 2.0**31)

    def __init__(self, latitude, longitude, speed, heart_rate, distance):
        """
        Init method
        Parameters
        ----------
        coordinate : dict
            The fit coordinates of the point
        decimal_coordinate : dict
            The decimal coordinates of the point
        speed : float
            The speed (in km/h) recorded at this point
        heart_rate : int
            The heart rate recorded at this point
        distance : float
            The distance covered at this point
        """
        self.coordinate = {"latitude": None, "Longitude": None}
        self.decimal_coordinate = {"latitude": None, "Longitude": None}
        self.coordinate["latitude"] = latitude
        self.coordinate["longitude"] = longitude
        if latitude is not None:
            self.decimal_coordinate["latitude"] = self._conv_fit_coord_to_dec_coord(
                latitude
            )
            self.decimal_coordinate["longitude"] = self._conv_fit_coord_to_dec_coord(
                longitude
            )
        self.speed = float(speed) * 3.6 if speed is not None else None
        self.heart_rate = heart_rate if heart_rate is not None else None
        self.distance = distance if distance is not None else None
