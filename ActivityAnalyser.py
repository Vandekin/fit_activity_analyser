import copy
import pandas as pd
import folium
import branca.colormap as cm

import RecordPoint


class ActivityAnalyser(object):
    """
    The analyser class.

    Attributes
    ----------
    fit_file : fitparse.FitFile
        The fit file to analyse
    sport : string
        The sport of the activity
    sub_sport : string
        The sub sport of the activity (for instance, open_water is a sub sport of swimming)
    has_hr : boolean
        True if the activity have HR
            Default : None
    total_time : string
        The total time of the activity
            Default : None
    recorded_average_speed : float
        The recorded average speed in km/h
            Default : None
    recorded_max_speed : float
        The recorded max speed in km/h
            Default : None
    recorded_average_heart_rate : int
        The recorded average heart rate in bpm if has_hr is True
            Default = None
    recorded_max_heart_rate : int
        The recorded max heart rate in bpm if has_hr is True
            Default = None
    record_point_list : list of RecordPoint
        The list of recorded points
            Default : empty list
    map : folium.Map
        The folium map used to represent the trace of the activity
            Default : None
    colormap_vmin : float
        The minimum speed of the color scale (used to print the trace on the map)
            Default : None
    colormap_vmax : float
        The maximum speed of the color scale (used to print the trace on the map)
            Default : None

    Methods
    -------
    get_coordinate_list() -> list of tuple(float, float)
        Return the decimal coordinates of the activity
    get_map_speed() -> list of float
        Return the list of speed associate with a coordinated point
    draw_map(is_map_to_save=True) -> None
        Draw the activity trace on a map and save it in html
    """

    def __init__(self, *args, **kwargs):
        """
        Init method
        """
        self.fit_file = kwargs["fit_file"]
        self.sport = kwargs["sport"]
        self.sub_sport = kwargs["sub_sport"]
        self.has_hr = None

        self.total_time = None
        self.recorded_average_speed = None
        self.recorded_max_speed = None
        self.recorded_average_heart_rate = None
        self.recorded_max_heart_rate = None

        self.record_point_list = []

        self.map = None
        self.colormap_vmin = None
        self.colormap_vmax = None

    def _init_data_from_file(self):
        """
        Read and initialize the fields of the the class
        """
        for message in self.fit_file.messages:
            if message.name == "record":
                record_point = RecordPoint.RecordPoint(
                    latitude=message.get_value("position_lat"),
                    longitude=message.get_value("position_long"),
                    speed=message.get_value("enhanced_speed"),
                    heart_rate=message.get_value("heart_rate"),
                    distance=message.get_value("distance"),
                )
                self.record_point_list.append(copy.deepcopy(record_point))
            elif message.name == "session":
                if message.get_value("event_type") == "stop":
                    self.total_time = message.get_value("total_timer_time")
                    self.recorded_average_speed = (
                        float(message.get_value("enhanced_avg_speed")) * 3.6
                    )
                    self.recorded_max_speed = (
                        float(message.get_value("enhanced_max_speed")) * 3.6
                    )
                    if self.has_hr:
                        self.recorded_average_heart_rate = int(
                            message.get_value("avg_heart_rate")
                        )
                        self.recorded_max_heart_rate = message.get_value(
                            "max_heart_rate"
                        )

    def get_coordinate_list(self):
        """
        Get the decimal coordinates of the activity

        Returns
        -------
        list of tuple(float, float)
            The list of points with decimal coordinates
        """
        decimal_coords = []
        for point in self.record_point_list:
            if point.coordinate["latitude"] is not None:
                decimal_coords.append(
                    (
                        point.decimal_coordinate["latitude"],
                        point.decimal_coordinate["longitude"],
                    )
                )
        return decimal_coords

    def get_map_speed(self):
        """
        Get the list of speed associate with a coordinated point

        Returns
        -------
        list of float
            The list of speed that match with a point
        """
        speed = []
        for point in self.record_point_list:
            if point.coordinate["latitude"] is not None:
                speed.append(point.speed)
        return speed

    def draw_map(self, is_map_to_save=True):
        """
        Draw the activity trace on a map and save it in html

        Parameters
        ----------
        is_map_to_save : boolean
            True if the trace need to be saved
                Default : True
        """
        # Get coordinates
        coord = pd.DataFrame(self.get_coordinate_list(), columns=["lat", "long"])
        # Compute the central point to center the map
        center_point_coord = (coord["lat"].mean(), coord["long"].mean())

        map_bounds = [
            coord[["lat", "long"]].max().values.tolist(),
            coord[["lat", "long"]].min().values.tolist(),
        ]

        # Get speed
        speed = self.get_map_speed()

        # Create folium map
        self.map = folium.Map(location=center_point_coord, zoom_start=16)
        # Fit map to trace size
        self.map.fit_bounds(map_bounds)
        # Add Start and End mark
        folium.Marker(
            location=(coord["lat"].iloc[0], coord["long"].iloc[0]),
            icon=folium.DivIcon(
                html="""<div style="font-family: courier new; color: blue">Start</div>"""
            ),
        ).add_to(self.map)
        folium.Marker(
            location=(
                coord["lat"].iloc[len(coord) - 1],
                coord["long"].iloc[len(coord) - 1],
            ),
            icon=folium.DivIcon(
                html="""<div style="font-family: courier new; color: blue">End</div>"""
            ),
        ).add_to(self.map)

        # Add speed legend to map
        colormap = cm.LinearColormap(
            ["red", "orange", "yellow", "green", "cyan", "blue", "magenta", "violet"],
            vmin=self.colormap_vmin,
            vmax=self.colormap_vmax,
        )
        colormap.caption = "Speed (km/h)"
        colormap.add_to(self.map)

        folium.ColorLine(
            positions=coord, colors=speed, colormap=colormap, weight=3
        ).add_to(self.map)

        if is_map_to_save:
            self.map.save("map.html")


class OpenWaterSwim(ActivityAnalyser):
    """
    The analyser class for the Open Water Swimming sport

    Attributes
    ----------
    colormap_vmin : float
        The minimum speed of the color scale (used to print the trace on the map)
            Default : 0.0
    colormap_vmax : float
        The maximum speed of the color scale (used to print the trace on the map)
            Default : 7.0
    """

    def __init__(self, *args, **kwargs):
        """
        Parameters
        ----------
        colormap_vmin : float
            The minimum speed of the color scale (used to print the trace on the map)
                Default : 0.0
        colormap_vmax : float
            The maximum speed of the color scale (used to print the trace on the map)
                Default : 7.0
        """
        super().__init__(*args, **kwargs)

        self.colormap_vmin = float(kwargs["vmin"]) if "vmin" in kwargs else 0.0
        self.colormap_vmax = float(kwargs["vmax"]) if "vmax" in kwargs else 7.0

        for message in self.fit_file.messages:
            if message.name == "device_info":
                device_type = message.get_value("device_type")
                if device_type == "heart_rate":
                    self.has_hr = message.get_value("source_type") != "local"
                break

        self._init_data_from_file()
